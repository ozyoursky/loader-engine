package com.vebry

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.http.ContentType.Text.Plain
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing

fun main(args: Array<String>) = io.ktor.server.cio.EngineMain.main(args)

@Suppress("unused")
@JvmOverloads
fun Application.module(testing: Boolean = false) {
    routing {
        get("/") {
            call.respondText("HELLO WORLD!", contentType = Plain)
        }
    }
}

